// const 
const IS_CLOCKWISE = true;
const
  VERTICAL_SIZE = 5,
  HORIZONTAL_SIZE = 5,
  AREA = VERTICAL_SIZE * HORIZONTAL_SIZE,
  CENTER = Math.round(AREA / 2),
  TOP_DIRECTION = 'top',
  RIGHT_DIRECTION = 'right',
  BOTTOM_DIRECTION = 'bottom',
  LEFT_DIRECTION = 'left',
  CLOCKWISE = [TOP_DIRECTION, RIGHT_DIRECTION, BOTTOM_DIRECTION, LEFT_DIRECTION],
  COUNTER_CLOCKWISE = [...CLOCKWISE].reverse(),
  DIRECTION_ENUM = IS_CLOCKWISE ? CLOCKWISE : COUNTER_CLOCKWISE;

// user consts
const
  output = [];


// task
let
  depth = 0,
  directionNum = 0,
  tickInLine = 0;


// const
//   NUMS = filledNums(AREA),
//   CENTER_NUM = NUMS.pop(),
//   FIRST_NUM = NUMS.shift();


// utils

const detectNumFillInRow = (depth, size) => {
  let result = depth > 0 ? size / (2 * depth) : size;
  return result % 2 === 0 && result || 1;
};

const fillInRow = (direction, depth) => {
  switch (direction) {
    case BOTTOM_DIRECTION :
    case TOP_DIRECTION :
      return detectNumFillInRow(depth, HORIZONTAL_SIZE - 1);
    case RIGHT_DIRECTION :
    case LEFT_DIRECTION :
      return detectNumFillInRow(depth, VERTICAL_SIZE - 1);
  }
};

const filledNums = (size) => {
  let arr = Array.from(new Array(size).keys());
  return IS_CLOCKWISE ? arr : arr.reverse();
};

const access = (matrix, depth, index) => {
  !matrix[depth] && (matrix[depth] = []);
  !matrix[depth][index] && (matrix[depth][index] = []);
};

const isLastDirection = direction => (
  direction > DIRECTION_ENUM.length - 1
);

// reset , and increment depth
const goDeeper = (direction, depth) => (
  [0, depth + 1]
);

const isLineFilled = (filledCols, directionNum, depth) => (
  filledCols === 0
    ? fillInRow(DIRECTION_ENUM[directionNum], depth)
    : filledCols
)

const append = ({
                  output,
                  depth,
                  directionNum: direction,
                  num,
                }) => {
  access(output, depth, direction);
  output[depth][direction].push(num);
}

const fill = (depth, directionNum, output,tickInLine) => (num) => {

  // change direction
  if (isLastDirection(directionNum)) {
    [directionNum, depth] = goDeeper(directionNum,depth);
  }

  tickInLine = isLineFilled(tickInLine, directionNum, depth);

  append({
    output,
    depth,
    directionNum,
    num
  });

  --tickInLine;

  if (tickInLine === 0) {
    directionNum++;
  }
}

(filledNums(AREA)).forEach(fill(depth, directionNum, output, tickInLine));


console.log('------------- output ', output);